//
//  ViewController.m
//  AdMobTest
//
//  Created by Andrew on 30.07.15.
//  Copyright (c) 2015 terenn.com. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) GADInterstitial *interstitial;//declare Interstitial

@end

@implementation ViewController

static NSString * const kUnitID = @"ca-app-pub-3940256099942544/2934735716";

- (void)loadView {
    _rootView = [RootView new];
    self.view = _rootView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [_rootView.displayAdsBtn addTarget:self action:@selector(lookInterstitial) forControlEvents:UIControlEventTouchUpInside];
    
    self.interstitial = [self createAndLoadInterstitial];//call method which initializes Interstitial and sends Request
    
    _rootView.bannerView.adUnitID = kUnitID;
    _rootView.bannerView.rootViewController = self;
    _rootView.bannerView.delegate = self;
    GADRequest *request = [GADRequest request];
    [_rootView.bannerView loadRequest:request];
}

//Method which show interstitial
- (void)lookInterstitial {
    if ([self.interstitial isReady]) {
        [self.interstitial presentFromRootViewController:self];
    }
}

//Method which initializes Interstitial and sends Request
- (GADInterstitial *)createAndLoadInterstitial {
    GADInterstitial *interstitial = [[GADInterstitial alloc] initWithAdUnitID:kUnitID];
    interstitial.delegate = self;
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ kGADSimulatorID ];
    [interstitial loadRequest:request];
    return interstitial;
}

#pragma mark - GADInterstitialDelegate

//Method which reload ad when the interstitial dismissed
- (void)interstitialDidDismissScreen:(GADInterstitial *)interstitial {
    self.interstitial = [self createAndLoadInterstitial];
}

#pragma mark - GADBannerViewDelegate

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    [UIView animateWithDuration:1.
                     animations:^{
                         bannerView.frame = CGRectMake((self.view.frame.size.width - bannerView.frame.size.width)/2,
                                                       self.view.frame.size.height - bannerView.frame.size.height,
                                                       bannerView.frame.size.width,
                                                       bannerView.frame.size.height);
                     }];
}

- (void) adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError:%@", [error localizedDescription]);
}

@end
