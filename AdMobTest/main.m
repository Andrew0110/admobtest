//
//  main.m
//  AdMobTest
//
//  Created by Andrew on 30.07.15.
//  Copyright (c) 2015 terenn.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
