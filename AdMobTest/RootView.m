//
//  RootView.m
//  AdMobTest
//
//  Created by Andrew on 01.08.15.
//  Copyright (c) 2015 terenn.com. All rights reserved.
//

#import "RootView.h"


@implementation RootView

- (instancetype)init {
    self = [super init];
    if (self) {
        _bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
        [self addSubview:_bannerView];
        
        _displayAdsBtn = [[UIButton alloc] init];
        _displayAdsBtn.backgroundColor = [UIColor purpleColor];
        [_displayAdsBtn setTitle:@"Look Advert" forState:UIControlStateNormal];
        [self addSubview:_displayAdsBtn];
    }
    return self;
}

- (void)layoutSubviews {
    _bannerView.frame = CGRectMake(self.bounds.size.width / 2 - _bannerView.adSize.size.width/2, self.bounds.size.height , _bannerView.adSize.size.width, _bannerView.adSize.size.height);
    
    _displayAdsBtn.frame = CGRectMake(100, 100, self.bounds.size.width-200, 100);
}

@end
