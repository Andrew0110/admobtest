//
//  ViewController.h
//  AdMobTest
//
//  Created by Andrew on 30.07.15.
//  Copyright (c) 2015 terenn.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootView.h"

@interface ViewController : UIViewController <GADInterstitialDelegate, GADBannerViewDelegate>

@property (nonatomic) RootView* rootView;

@end

