//
//  RootView.h
//  AdMobTest
//
//  Created by Andrew on 01.08.15.
//  Copyright (c) 2015 terenn.com. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface RootView : UIView

@property (nonatomic) GADBannerView* bannerView;
@property (nonatomic) UIButton* displayAdsBtn;

@end
